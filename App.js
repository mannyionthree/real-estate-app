import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {StackNavigator} from 'react-navigation';

//importing screens
import HomeScreen from './Screens/HomeScreen';
import FoyerScreen from './Screens/FoyerScreen';
import KitchenScreen from './Screens/KitchenScreen';
import LivingRoomScreen from './Screens/LivingRoomScreen';


export default class App extends React.Component {
  render() {
    return (
      <ScreenNavigation/>
    );
  }
}

const ScreenNavigation = StackNavigator({
  HomeScreen: {screen : HomeScreen},
  FoyerScreen: {screen : FoyerScreen},
  KitchenScreen: {screen : KitchenScreen},
  LivingRoomScreen: {screen: LivingRoomScreen}
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
