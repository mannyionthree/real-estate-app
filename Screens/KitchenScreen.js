import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Background from '../Components/Background'

class KitchenScreen extends Component {
    render() {
        return (
            <Background source={require("../assets/kitchen.jpg")} title="Welcome to Kitchen" place="kitchen"/>
        );
    }
}

export default KitchenScreen;