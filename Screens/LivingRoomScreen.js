import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Background from '../Components/Background'

class LivingRoomScreen extends Component {
    render() {
        return (
            <Background source={require("../assets/livingroom.jpg")} title="Welcome to Living Room" place="livingroom"/>
        );
    }
}

export default LivingRoomScreen;