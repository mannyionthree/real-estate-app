import React, { Component } from "react";
import { View, Text, StyleSheet, ImageBackground } from "react-native";
import Background from '../Components/Background'

class FoyerScreen extends Component {
    constructor(){
        super()
        this.modalContent = this.modalContent.bind(this)
    }
    modalContent(){
        <View>
            this content is coming from foyer
        </View>
    }
  render() {
    return (
        <Background source={require("../assets/foyer.jpg")} title="Welcome to Foyer" place="foyer"/>
    );
  }
}
export default FoyerScreen;
