import React, { Component } from "react";
import { View, ScrollView,Text, StyleSheet } from "react-native";
import { Button, Card } from "react-native-elements";

class HomeScreen extends Component {
  render() {
    return (
      <ScrollView style={{backgroundColor: "white"}}>
        <Card title="Foyer" image={require("../assets/foyer.jpg")}>
          <Button
            small
            raised
            backgroundColor="black"
            color="white"
            onPress={() => this.props.navigation.navigate("FoyerScreen")}
            title="View"
          />
        </Card>
        <Card title="Kitchen" image={require("../assets/kitchen.jpg")}>
          <Button
            small
            raised
            backgroundColor="black"
            color="white"
            onPress={() => this.props.navigation.navigate("KitchenScreen")}
            title="View"
          />
        </Card>
        <Card title="Living Room" image={require("../assets/livingroom.jpg")}>
          <Button
            small
            raised
            backgroundColor="black"
            color="white"
            onPress={() => this.props.navigation.navigate("LivingRoomScreen")}
            title="View"
          />
        </Card>
      </ScrollView>
    );
  }
}

export default HomeScreen;

const style = StyleSheet.create({});
