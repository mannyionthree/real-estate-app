import React, {Component} from 'react'
import { View, Text, StyleSheet, ImageBackground, Dimensions } from "react-native";
import FooterBar from '../Components/FooterBar'


class Background extends Component {
    constructor(props){
        super(props)
        this.state = {
            screen: this.props.place
        }
    }
    render () {
        return (
            <ImageBackground
            source={this.props.source}
            style={style.backgroundImage}>
            <Text style={style.title}>{this.props.title}</Text>
            <FooterBar room={this.state.screen}/>
          </ImageBackground>
        );
    }
}
let deviceWidth = Dimensions.get('window').width
const style = StyleSheet.create({
    backgroundImage: {
      flex: 1,
      width: null,
      height: null
    },
    title: {
      textAlign: "center",
      fontWeight: "bold",
      fontSize: deviceWidth * 0.080
    }
  });

export default Background