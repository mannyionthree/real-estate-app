import React, { Component } from "react";
import { View, StyleSheet, TouchableOpacity, Text} from "react-native";
import Modal from "react-native-modal";

export default class FeatureModal extends Component {
    constructor(props){
        super(props)
        this.state = {
            modalOpen: false
        }
    }
  render() {
    return (
      <View style={style.modalContainer}>
        <Modal
          isVisible={this.props.modalState}
        >
          <View style={style.modalContainer}>
            <Text>Modal is open</Text>
            <View>{this.props.content}</View>
            <TouchableOpacity
              onPress={() => this.setState({ modalOpen: false })}
            >
              <Text>Close Modal</Text>
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    );
  }
}

const style = StyleSheet.create({
    modalContainer: {
        flex: 1,
        flexDirection: "column",
        justifyContent: 'center',
        alignItems: "center",
        margin: 40
    }
})