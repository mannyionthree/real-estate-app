import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { Ionicons, FontAwesome } from "@expo/vector-icons";
//import FeatureModal from "./FeatureModal";
import Modal from "react-native-modal";
import { Icon, Button, ListItem } from "react-native-elements";

class FooterBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false
    };
    this.toggleModal = this.toggleModal.bind(this);
  }
  toggleModal() {
    this.setState({
      showModal: !this.state.showModal
    });
  }
  render() {
    var content;
    if (this.props.room === "foyer") {
      content = (
        <View>
    </View>
      );
    } else if (this.props.room === "kitchen") {
      content = (
        <View>
          <Text>This is coming from kitchen</Text>
        </View>
      );
    } else if (this.props.room === "livingroom") {
      content = (
        <View>
          <Text>This is coming from livingroom</Text>
        </View>
      );
    }
    return (
      <View style={style.container}>
        <Modal isVisible={this.state.showModal}
        onBackButtonPress={this.toggleModal}
        onBackdropPress={this.toggleModal}
        onSwipe={this.toggleModal}
        swipeDirection="left">
          <View style={style.modalContainer}>
            <TouchableOpacity
              onPress={this.toggleModal}
            >
              <Icon
                raised
                small
                name="close"
                style={{ justifyContent: "space-between" }}
              />
            </TouchableOpacity>
            <View>
            {
             list.map((l, i) => (
             <ListItem
                 key={i}
                 title={l.item}
                 style={{width:"100%", color: "black"}}
             />
            ))
            }  
            </View>

          </View>
        </Modal>
        <View style={style.footerContainer}>
          <TouchableOpacity onPress={this.toggleModal} style={style.flexCol}>
            <Text>
              <Ionicons name="md-information-circle" size={32} color="black" />
            </Text>
            <Text>Features</Text>
          </TouchableOpacity>
          <TouchableOpacity style={style.flexCol}>
            <Text>
              <Ionicons name="ios-mic" size={32} color="black" />
            </Text>
            <Text>Add a Memo</Text>
          </TouchableOpacity>
          <TouchableOpacity style={style.flexCol}>
            <Text>
              <Ionicons name="md-camera" size={32} color="black" />
            </Text>
            <Text>Add a Photo</Text>
          </TouchableOpacity>
          <TouchableOpacity style={style.flexCol}>
            <Text>
              <Ionicons name="md-volume-up" size={32} color="black" />
            </Text>
            <Text>Audio</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}


const list = [
    {
        item: "20ft High Vaulted Ceilings"
    },
    {
        item: "Steel Double Entry Foyer Doors"
    },
    {
        item: "Solid Hardwood Flooring"
    },
    {
        item: "Natural Open Lighting"
    },
    {
        item: "6 inch Wood Trim"
    },
    {
        item: "Solid Hardwood Entry Way Staircase"
    }
]

const style = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row"
  },
  footerContainer: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: "white",
    position: "absolute",
    left: 0,
    bottom: 0,
    width: "100%"
  },
  flexCol: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    margin: 15
  },
  modalContainer: {
    width: "100%",
    height: "70%",
    backgroundColor: "white",
    position: "absolute",
    alignItems: "center"
  }
});

export default FooterBar;
